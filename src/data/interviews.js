export default [
  {
    meta: 'You are techY Podcast',
    title: 'Interview with Ellen Twomey',
    path: 'https://share.transistor.fm/s/51b9c2f2',
  },
  {
    meta: 'SiteMonster',
    title: 'The Secret Is Just Do a Little Bit Over a Long Time',
    path:
      'https://sitesmonster.com/interviews/tania-rascia-the-secret-is-just-do-a-little-bit-over-a-long-time',
  },
  {
    meta: 'Egghead.io Podcast',
    title: 'Switching Careers and Learning in Public with Joel Hooks',
    path:
      'https://egghead.io/podcasts/switching-careers-and-learning-in-public-with-tania-rascia',
  },
  {
    meta: 'Hashnode',
    title: 'She Inspires with Bolaji Ayodeji',
    path:
      'https://townhall.hashnode.com/women-in-tech-tania-rascia-ck2bfzn2100up3is10zhphol4',
  },
]
